/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android

import android.app.Application
import app.umcu.android.api.HttpService
import app.umcu.android.api.umcu.UMCU
import app.umcu.android.api.umcu.UMCUImpl
import app.umcu.android.screen.HomeViewModel
import coil.ImageLoader
import coil.ImageLoaderFactory
import coil.disk.DiskCache
import coil.memory.MemoryCache
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.context.startKoin
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import timber.log.Timber

class MainApplication : Application(), ImageLoaderFactory {

	override fun onCreate() {
		super.onCreate()

		Timber.plant(Timber.DebugTree())

		startKoin {
			androidLogger()
			androidContext(this@MainApplication)
			// TODO: Move this to a separate file
			modules(module {
				singleOf(::HttpService)
				single<UMCU> { UMCUImpl(get()) }
				viewModelOf(::HomeViewModel)
			})
		}
	}

	override fun newImageLoader() = ImageLoader.Builder(applicationContext).memoryCache {
		MemoryCache.Builder(applicationContext).maxSizePercent(0.25).build()
	}.diskCache {
		DiskCache.Builder().directory(applicationContext.cacheDir.resolve("image_cache"))
			.maxSizePercent(0.02).build()
	}.build()
}
