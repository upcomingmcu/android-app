/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.ui.nav

import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import app.umcu.android.AppState
import app.umcu.android.screen.HomeView
import app.umcu.android.screen.SettingsView

@Composable
fun UMCUNavHost(modifier: Modifier = Modifier, appState: AppState) {
	val enterTransition = EnterTransition.None
	val exitTransition = ExitTransition.None

	NavHost(modifier = modifier,
		navController = appState.navHostController,
		startDestination = Destination.Home.route,
		enterTransition = { enterTransition },
		exitTransition = { exitTransition },
		popEnterTransition = { enterTransition },
		popExitTransition = { exitTransition }) {
		composable(route = Destination.Home.route) { HomeView(appState = appState) }
		composable(route = Destination.Settings.route) { SettingsView(appState = appState) }
	}
}
