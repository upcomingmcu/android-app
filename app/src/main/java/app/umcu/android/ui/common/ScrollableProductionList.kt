/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.ui.common

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import app.umcu.android.R
import app.umcu.android.api.umcu.Production

@Composable
fun ScrollableProductionList(
	modifier: Modifier = Modifier,
	productions: List<Production>,
	posterWidth: Dp = 300.dp,
	spacing: Dp = 16.dp,
	shadowElevation: Dp = 0.dp,
) {
	ScrollableProductionListRow(
		modifier = modifier,
		productions = productions,
		posterWidth = posterWidth,
		spacing = spacing,
		shadowElevation = shadowElevation
	)
}

@Composable
fun ScrollableProductionListWithTitle(
	modifier: Modifier = Modifier,
	productions: List<Production>,
	posterWidth: Dp = 300.dp,
	spacing: Dp = 16.dp,
	shadowElevation: Dp = 0.dp,
	title: @Composable () -> Unit,
) {
	Column(
		modifier = modifier,
		verticalArrangement = Arrangement.spacedBy(spacing / 2, Alignment.CenterVertically),
		horizontalAlignment = Alignment.Start
	) {
		Box(modifier = Modifier.padding(horizontal = spacing)) { title() }
		ScrollableProductionListRow(
			productions = productions,
			posterWidth = posterWidth,
			spacing = spacing,
			shadowElevation = shadowElevation
		)
	}
}

@Composable
private fun ScrollableProductionListRow(
	modifier: Modifier = Modifier,
	productions: List<Production>,
	posterWidth: Dp,
	spacing: Dp,
	shadowElevation: Dp,
) {
	LazyRow(
		modifier = modifier,
		horizontalArrangement = Arrangement.spacedBy(spacing),
		verticalAlignment = Alignment.CenterVertically
	) {
		item { }
		itemsIndexed(
			items = productions,
			key = { index, item -> "${item.slug}_${index}" }) { _, production ->
			Poster(production = production, width = posterWidth, shadowElevation = shadowElevation)
		}
		item { }
	}
}

@Preview(name = "Light")
//@Preview(name = "Dark", uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun ScrollableMovieListPreview() {
	val productions = List(10) { Production.SAMPLE }
	MaterialTheme {
		ScrollableProductionList(productions = productions)
	}
}

@Preview(name = "Light")
//@Preview(name = "Dark", uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun ScrollableProductionListWithTitlePreview() {
	val productions = List(10) { Production.SAMPLE }
	MaterialTheme {
		ScrollableProductionListWithTitle(productions = productions) {
			Text(
				text = stringResource(id = R.string.app_name),
				style = MaterialTheme.typography.headlineLarge
			)
		}
	}
}
