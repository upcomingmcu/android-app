/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.ui.common

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import app.umcu.android.R
import app.umcu.android.api.umcu.Production
import coil.compose.SubcomposeAsyncImage

@Composable
fun Poster(
	modifier: Modifier = Modifier,
	production: Production,
	width: Dp = 500.dp,
	height: Dp = (width.value * 1.5).dp,
	shape: Shape = RoundedCornerShape(4.dp),
	shadowElevation: Dp = 1.dp,
) {
	val posterUrl = if (production.posterPath == null) null else "https://image.tmdb.org/t/p/w500/${
		production.posterPath.drop(1)
	}"

	val placeholder =
		@Composable { PosterPlaceholder(width = width, height = height, shape = shape) }

	SubcomposeAsyncImage(modifier = modifier
		.width(width)
		.height(height)
		.shadow(elevation = shadowElevation, shape = shape)
		.clip(shape),
		model = posterUrl,
		contentDescription = production.title,
		contentScale = ContentScale.Crop,
		loading = { placeholder() },
		error = { placeholder() })
}

@Composable
private fun PosterPlaceholder(
	modifier: Modifier = Modifier,
	width: Dp,
	height: Dp = (width.value * 1.5).dp,
	shape: Shape,
) {
	val containerColor = MaterialTheme.colorScheme.background
	val contentColor = contentColorFor(backgroundColor = containerColor)
	val tonalElevation = 1.dp

	Surface(
		modifier = modifier,
		shape = shape,
		color = containerColor,
		contentColor = contentColor,
		tonalElevation = tonalElevation
	) {
		Box(
			modifier = modifier
				.width(width)
				.height(height), contentAlignment = Alignment.Center
		) {
			Icon(
				painter = painterResource(id = R.drawable.question_mark),
				contentDescription = null,
				tint = contentColor,
				modifier = Modifier.size(width / 4)
			)
		}
	}
}

@Preview(name = "Light")
@Preview(name = "Dark", uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun PosterPreview() {
	MaterialTheme {
		Poster(production = Production.SAMPLE)
	}
}
