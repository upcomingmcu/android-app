/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.ui.main

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.systemBars
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import app.umcu.android.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DefaultScaffold(
	modifier: Modifier = Modifier,
	@StringRes title: Int? = null,
	showNavLogo: Boolean = true,
	@DrawableRes navLogo: Int? = R.drawable.logo,
	onBack: (() -> Unit)? = null,
	actions: @Composable RowScope.() -> Unit = {},
	scrollBehavior: TopAppBarScrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior(),
	contentWindowInsets: WindowInsets = WindowInsets.systemBars,
	content: @Composable (PaddingValues) -> Unit,
) {
	val topBar: @Composable () -> Unit = {
		TopAppBar(scrollBehavior = scrollBehavior, title = {
			Row(
				horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
				verticalAlignment = Alignment.CenterVertically
			) {
				if (showNavLogo && navLogo != null) Icon(
					painterResource(id = navLogo),
					contentDescription = null,
					tint = Color.Unspecified
				)
				if (title != null) Text(text = stringResource(id = title))
			}
		}, navigationIcon = {
			if (onBack != null) BackButton(onClick = onBack)
		}, actions = actions)
	}

	Scaffold(
		modifier = modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
		topBar = topBar,
		contentWindowInsets = contentWindowInsets,
		content = content
	)
}

@Composable
private fun BackButton(onClick: () -> Unit) {
	IconButton(onClick = onClick) {
		Icon(
			imageVector = Icons.AutoMirrored.Default.ArrowBack,
			contentDescription = stringResource(R.string.back)
		)
	}
}
