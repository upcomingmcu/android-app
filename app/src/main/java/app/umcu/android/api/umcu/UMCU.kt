/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.api.umcu

import app.umcu.android.api.ApiError
import app.umcu.android.api.ApiResponse
import app.umcu.android.api.umcu.UMCUFilter.ALL
import app.umcu.android.api.umcu.UMCUFilter.ANNOUNCED
import app.umcu.android.api.umcu.UMCUFilter.RELEASED
import app.umcu.android.api.umcu.UMCUFilter.UPCOMING

/**
 * @property ALL All productions.
 * @property RELEASED Productions that have been released.
 * @property UPCOMING Productions that are scheduled to be released.
 * @property ANNOUNCED Productions with no set release date.
 */
enum class UMCUFilter {

	ALL, RELEASED, UPCOMING, ANNOUNCED;

	override fun toString(): String {
		return name.lowercase()
	}
}

interface UMCU {

	/**
	 * Get all productions.
	 *
	 * @param [filter] Which productions should be returned.
	 */
	suspend fun getProductions(filter: UMCUFilter? = null): ApiResponse<ProductionList, ApiError>

	/**
	 * Get a single production by its slug.
	 *
	 * @param [slug] The desired production's slug.
	 */
	suspend fun getProduction(slug: String): ApiResponse<Production, ApiError>

	/**
	 * Get the next production to-be-released.
	 *
	 * @param dateString (Optional) Get the production that releases after this date.
	 */
	suspend fun getNextProduction(dateString: String? = null): ApiResponse<NextProduction, ApiError>
}
