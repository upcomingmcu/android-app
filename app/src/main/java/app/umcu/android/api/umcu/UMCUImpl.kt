/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.api.umcu

import app.umcu.android.api.ApiError
import app.umcu.android.api.ApiResponse
import app.umcu.android.api.HttpService

class UMCUImpl(private val httpService: HttpService) : UMCU {

	companion object {

		private const val BASE_URL = "https://umcu.app/api"
	}

	/**
	 * Get all productions.
	 *
	 * @param [filter] Which productions should be returned.
	 */
	override suspend fun getProductions(filter: UMCUFilter?): ApiResponse<ProductionList, ApiError> {
		return httpService.req<ProductionList>(
			baseUrl = BASE_URL,
			pathSegments = arrayOf("productions"),
			parameters = arrayOf(Pair("filter", filter?.toString() ?: UMCUFilter.ALL.toString()))
		)
	}

	/**
	 * Get a single production by its slug.
	 *
	 * @param [slug] The desired production's slug.
	 */
	override suspend fun getProduction(slug: String): ApiResponse<Production, ApiError> {
		return httpService.req<Production>(
			baseUrl = BASE_URL, pathSegments = arrayOf("productions", slug)
		)
	}

	/**
	 * Get the next production to-be-released.
	 *
	 * @param dateString (Optional) Get the production that releases after this date.
	 */
	override suspend fun getNextProduction(dateString: String?): ApiResponse<NextProduction, ApiError> {
		return httpService.req<NextProduction>(
			baseUrl = BASE_URL,
			pathSegments = arrayOf("productions", "next"),
			parameters = arrayOf(Pair("date", dateString ?: ""))
		)
	}
}
