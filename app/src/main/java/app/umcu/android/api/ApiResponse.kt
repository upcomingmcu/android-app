/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.api

sealed class ApiResponse<out D, out E : ApiError> {

	/**
	 * Indicates the response from the API was successful.
	 *
	 * @param D The type of the [data]
	 * @param data The data retrieved.
	 */
	data class Success<out D>(val data: D) : ApiResponse<D, Nothing>()

	/**
	 * Indicates the response from the API failed.
	 * This could be because of numerous factors that should be
	 * noted by the [error] parameter.
	 *
	 * @param E The type of the [error]
	 * @param error The error that caused a failure.
	 */
	data class Failure<out E : ApiError>(val error: E) : ApiResponse<Nothing, E>()

	/**
	 * Indicates that the response is loading and has not succeeded or failed.
	 */
	data object Loading : ApiResponse<Nothing, Nothing>()
}
