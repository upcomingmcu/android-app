/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.api

import io.ktor.http.HttpStatusCode

sealed class ApiError {

	/**
	 * The API responded with an unsuccessful HTTP code (4xx - 5xx).
	 * Catches [io.ktor.client.plugins.ClientRequestException] or [io.ktor.client.plugins.ServerResponseException].
	 */
	data class HttpError(val status: HttpStatusCode) : ApiError()

	/**
	 * The HTTP client could not reach the internet.
	 * Catches [io.ktor.utils.io.errors.IOException].
	 */
	data class NetworkError(val throwable: Throwable) : ApiError()

	/**
	 * The JSON data could not be converted into the provided data model.
	 * Catches [io.ktor.serialization.JsonConvertException].
	 */
	data class JsonConvertError(val throwable: Throwable) : ApiError()

	/**
	 * The data could not be serialized into the provided data model.
	 * Catches [kotlinx.serialization.SerializationException].
	 */
	data class SerializationError(val throwable: Throwable) : ApiError()

	/**
	 * An undetermined error occurred.
	 * If this occurs then the maintainer should add a new ApiError subclass.
	 * Catches any generic [Throwable].
	 */
	data class GenericError(val throwable: Throwable) : ApiError()
}
