/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.api.umcu

import kotlinx.serialization.Serializable

@Serializable
data class ProductionList(val count: Int, val results: List<Production>)

@Serializable
data class Production(
	val slug: String,
	val tmdbId: Int,
	val imdbId: String?,
	val title: String,
	val releaseDate: String?,
	val overview: String?,
	val posterPath: String?,
	val mediaType: String,
) {

	companion object {

		val SAMPLE = Production(
			slug = "avengers-endgame",
			tmdbId = 299534,
			imdbId = "tt4154796",
			title = "Avengers: Endgame",
			releaseDate = "2019-04-26T04:00:00Z",
			overview = "After the devastating events of Avengers: Infinity War, the universe is in ruins...",
			posterPath = "/or06FN3Dka5tukK1e9sl16pB3iy.jpg",
			mediaType = "movie"
		)
	}
}

@Serializable
data class NextProduction(val production: Production, val previous: String?, val next: String?)
