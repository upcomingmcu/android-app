/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.api

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.plugins.ServerResponseException
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.parameter
import io.ktor.client.request.request
import io.ktor.http.HttpMethod
import io.ktor.http.appendPathSegments
import io.ktor.serialization.JsonConvertException
import io.ktor.serialization.kotlinx.json.json
import io.ktor.utils.io.errors.IOException
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonNamingStrategy
import timber.log.Timber

class HttpService {

	@OptIn(ExperimentalSerializationApi::class)
	val client = HttpClient(CIO) {
		install(ContentNegotiation) {
			json(Json {
				explicitNulls = false
				ignoreUnknownKeys = true
				namingStrategy = JsonNamingStrategy.SnakeCase
			})
		}

		install(Logging) {
			logger = object : Logger {
				override fun log(message: String) {
					Timber.d(message)
				}
			}
			level = LogLevel.INFO
		}
	}

	suspend inline fun <reified T : Any> req(
		method: HttpMethod = HttpMethod.Get,
		baseUrl: String,
		pathSegments: Array<String> = emptyArray(),
		parameters: Array<Pair<String, String>> = emptyArray(),
	): ApiResponse<T, ApiError> {
		val response = try {
			client.request(baseUrl) {
				this.method = method
				url {
					appendPathSegments(*pathSegments)
					parameters.forEach { pair -> parameter(pair.first, pair.second) }
				}
			}
		} catch (e: ClientRequestException) {
			Timber.e(e)
			return ApiResponse.Failure(ApiError.HttpError(e.response.status))
		} catch (e: ServerResponseException) {
			Timber.e(e)
			return ApiResponse.Failure(ApiError.HttpError(e.response.status))
		} catch (e: IOException) {
			Timber.e(e)
			return ApiResponse.Failure(ApiError.NetworkError(e))
		} catch (t: Throwable) {
			Timber.e(t)
			return ApiResponse.Failure(ApiError.GenericError(t))
		}

		return try {
			ApiResponse.Success(response.body<T>())
		} catch (e: JsonConvertException) {
			Timber.e(e)
			return ApiResponse.Failure(ApiError.JsonConvertError(e))
		} catch (e: SerializationException) {
			Timber.e(e)
			return ApiResponse.Failure(ApiError.SerializationError(e))
		} catch (t: Throwable) {
			Timber.e(t)
			return ApiResponse.Failure(ApiError.GenericError(t))
		}
	}
}
