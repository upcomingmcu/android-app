/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.screen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import app.umcu.android.AppState
import app.umcu.android.R
import app.umcu.android.ui.main.DefaultScaffold

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsView(appState: AppState) {
	DefaultScaffold(title = R.string.dest_settings,
		showNavLogo = false,
		onBack = { appState.back() }) {
		SettingsViewContent(modifier = Modifier.padding(paddingValues = it))
	}
}

@Composable
private fun SettingsViewContent(modifier: Modifier = Modifier) {
	Box(modifier = modifier.fillMaxSize()) { }
}
