/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.screen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.umcu.android.api.ApiError
import app.umcu.android.api.ApiResponse
import app.umcu.android.api.trimDateStringFromUrl
import app.umcu.android.api.umcu.NextProduction
import app.umcu.android.api.umcu.UMCU
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

data class HomeUiState(val response: ApiResponse<NextProduction, ApiError> = ApiResponse.Loading)

// TODO: Nothing in this class is final. This was done to ensure logic was working and the idea works.
class HomeViewModel(private val umcu: UMCU) : ViewModel() {

	private val _uiState = MutableStateFlow(HomeUiState())
	val uiState = _uiState.asStateFlow()

	fun getProduction(dateString: String? = null): Job {
		_uiState.update { HomeUiState(response = ApiResponse.Loading) }
		return viewModelScope.launch {
			val response = umcu.getNextProduction(dateString = dateString)
			_uiState.update { HomeUiState(response = response) }
		}
	}

	fun getPreviousProduction() {
		val currentProduction = (_uiState.value.response as? ApiResponse.Success)?.data
		val previousDateString = currentProduction?.previous?.trimDateStringFromUrl()
		getProduction(dateString = previousDateString)
	}

	fun getNextProduction() {
		val currentProduction = (_uiState.value.response as? ApiResponse.Success)?.data
		val nextDateString = currentProduction?.next?.trimDateStringFromUrl()
		getProduction(dateString = nextDateString)
	}
}
