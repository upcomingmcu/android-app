/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.KeyboardArrowLeft
import androidx.compose.material.icons.automirrored.filled.KeyboardArrowRight
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilledIconButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import app.umcu.android.AppState
import app.umcu.android.R
import app.umcu.android.api.ApiResponse
import app.umcu.android.api.umcu.NextProduction
import app.umcu.android.ui.main.DefaultScaffold
import app.umcu.android.ui.nav.Destination
import org.koin.androidx.compose.koinViewModel

/**
 * TODO: Nothing in this file is final. This was done to ensure logic was working and the idea works.
 */

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeView(appState: AppState) {
	val model: HomeViewModel = koinViewModel()

	LaunchedEffect(key1 = Unit) {
		model.getProduction()
	}

	val state by model.uiState.collectAsState()

	DefaultScaffold(title = R.string.dest_home,
		actions = { SettingsButton(onClick = { appState.navigate(Destination.Settings) }) }) {
		HomeViewContent(modifier = Modifier.padding(paddingValues = it),
			state = state,
			onPrevious = { model.getPreviousProduction() },
			onNext = { model.getNextProduction() })
	}
}

@Composable
private fun HomeViewContent(
	modifier: Modifier = Modifier,
	state: HomeUiState,
	onPrevious: () -> Unit,
	onNext: () -> Unit,
) {
	Column(
		modifier = modifier.fillMaxSize(),
		verticalArrangement = Arrangement.Center,
		horizontalAlignment = Alignment.CenterHorizontally
	) {
		when (val res = state.response) {
			is ApiResponse.Failure -> Text(text = res.error.toString())
			ApiResponse.Loading -> CircularProgressIndicator()
			is ApiResponse.Success -> Production(
				data = res.data, onPrevious = onPrevious, onNext = onNext
			)
		}
	}
}

@Composable
private fun SettingsButton(onClick: () -> Unit) {
	IconButton(onClick = onClick) {
		Icon(
			imageVector = Icons.Default.Settings,
			contentDescription = stringResource(R.string.dest_settings)
		)
	}
}

@Composable
private fun Production(
	modifier: Modifier = Modifier, data: NextProduction,
	onPrevious: () -> Unit,
	onNext: () -> Unit,
) {
	val production = data.production

	Column(
		modifier = modifier.fillMaxSize(),
		verticalArrangement = Arrangement.Center,
		horizontalAlignment = Alignment.CenterHorizontally
	) {
		Text(text = production.title)
		production.releaseDate?.let { Text(text = it) }

		Spacer(modifier = Modifier.height(12.dp))

		Row(
			modifier = modifier.fillMaxWidth(),
			horizontalArrangement = Arrangement.spacedBy(12.dp, Alignment.CenterHorizontally),
			verticalAlignment = Alignment.CenterVertically
		) {
			FilledIconButton(
				onClick = { if (data.previous != null) onPrevious() },
				enabled = data.previous != null
			) {
				Icon(
					imageVector = Icons.AutoMirrored.Filled.KeyboardArrowLeft,
					contentDescription = "previous production"
				)
			}

			FilledIconButton(
				onClick = { if (data.next != null) onNext() }, enabled = data.next != null
			) {
				Icon(
					imageVector = Icons.AutoMirrored.Filled.KeyboardArrowRight,
					contentDescription = "next production"
				)
			}
		}
	}
}
