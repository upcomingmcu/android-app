/*
 * Copyright (C) 2024 The UpcomingMCU authors
 * <https://gitlab.com/upcomingmcu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.android

import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import app.umcu.android.ui.nav.Destination

@Stable
class AppState(val navHostController: NavHostController) {

	fun navigate(destination: Destination, popBackStack: Boolean = false) {
		navHostController.navigate(destination.route) {
			if (popBackStack) {
				popUpTo(navHostController.graph.findStartDestination().id) {
					saveState = true
				}
			}
			launchSingleTop = true
			restoreState = true
		}
	}

	fun back() {
		navHostController.popBackStack()
	}
}

@Composable
fun rememberAppState(navHostController: NavHostController = rememberNavController()) =
	remember(navHostController) {
		AppState(navHostController = navHostController)
	}
